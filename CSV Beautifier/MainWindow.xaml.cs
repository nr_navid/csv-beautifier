﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace JavaScriptPageInjector
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Action<List<string>, bool,RichTextBox> appOutputAction;
        int space = 35;
        private readonly object timerlock = new object();
        public MainWindow()
        {
            InitializeComponent();
            appOutputAction = (texts, startover,richTextBox) =>
            {
                if (startover)
                {
                    richTextBox.Document.Blocks.Clear();
                }
                foreach(string text in texts)
                {
                    richTextBox.Document.Blocks.Add(new Paragraph(new Run(text)));
                }
            };
        }
        private string GenerateSpacedText(string text)
        {
            double des = (space - text.Length) / 2;
            double remaindspace = Math.Ceiling(des);
            string result = string.Empty;
            for(int i =1; i< remaindspace; i ++)
            {
                result += " ";
            }
            result += text;
            while(result.Length < space)
            {
                result += " ";
            }
            return result;

        }
        private void Extract_Js_Code(object sender, RoutedEventArgs e)
        {
            BeautifyCSV(txtInput);
        }
        private async void RichTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            //await Task.Run(() =>
            //{
            //    try
            //    {
            //        if (!new TextRange(txtInput.Document.ContentStart, txtInput.Document.ContentEnd).Text.StartsWith("input:"))
            //        {
            //            if (!(timer is null))
            //            {
            //                timer.Stop();
            //                timer.Start();
            //            }
            //            else
            //            {
            //                lock (timerlock)
            //                {
            //                    if (timer is null)
            //                    {
            //                        timer = new Timer(200);
            //                        timer.Enabled = true;
            //                        timer.AutoReset = false;
            //                        timer.Elapsed += async (sender, e) =>
            //                        {
            //                            await BeautifyCSV(txtInput);
            //                        };
            //                    }
            //                }
            //            }
            //        }
            //    }
            //    catch(Exception ex)
            //    {

            //    }
               
            //});
        }
        public async Task BeautifyCSV(RichTextBox richTextBox)
        {
            //var mainwindow = this;
            string originalText = string.Empty;
            originalText = new TextRange(richTextBox.Document.ContentStart, richTextBox.Document.ContentEnd).Text;
            await Task.Run(() =>
            {
                Dispatcher.Invoke(appOutputAction, new object[3] { new List<string>() { "Starting To Beutify The Csv For Readability" }, true, txtappoutput });
                string[] instruments = originalText.Split(";");
                Dispatcher.Invoke(appOutputAction, new object[3] { new List<string>() { "" }, false, txtoutputone });
                foreach (string instrument in instruments)
                {
                    string beutifiedcsv = string.Empty;
                    string[] insdetails = instrument.Split(',');
                    foreach(string instdetail in insdetails)
                    {
                        var instdetailin = instdetail.Replace(" ", " ");
                        if(space < instdetailin.Length)
                        {
                            Dispatcher.Invoke(appOutputAction, new object[3] { new List<string>() { "Not Goooooooooooooooood!!!!" }, true, txtappoutput });
                        }
                        int count = space - instdetailin.Length;
                        beutifiedcsv += $"{GenerateSpacedText(instdetailin)},";                
                    }
                    beutifiedcsv += $";";
                    beutifiedcsv = beutifiedcsv.Replace(",;", ";");
                    Dispatcher.Invoke(appOutputAction, new object[3] { new List<string>() { beutifiedcsv }, false, txtoutputone });
                }
            });
            Dispatcher.Invoke(appOutputAction, new object[3] { new List<string>() { "Finished The Job. :D" }, false, txtappoutput });
           
        }
        private async void Generate_WebPack_Script_Path(object sender, RoutedEventArgs e)
        {
          
        }
    }
}
